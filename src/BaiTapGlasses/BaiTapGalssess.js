import React, { Component } from "react";
import data from "./dataGlasses.json";
export default class BaiTapGalssess extends Component {
  state = { imgUrl: "", name: "", price: "", desc: "" };
  handleChoseGlasses = (url, index) => {
    let name = data[index].name;
    let price = data[index].price;
    let desc = data[index].desc;
    // let url = data[index].url;
    this.setState({
      imgUrl: url,
      name: name,
      price: price,
      desc: desc,
    });
    console.log(this.state.imgUrl);
  };
  render() {
    return (
      <div>
        <div
          className="bg-dark oppa text-center text-white py-5 "
          style={{ fontSize: "40px" }}
        >
          <div>TRY GLASSES APP ON</div>
        </div>
        <div className="display_show">
          <div>
            <img className="m-5 ab" src="./glassesImage/model.jpg"></img>
            <div className="pos">
              <img
                src={this.state.imgUrl}
                style={{
                  width: "250px",
                }}
                className="imgUrl"
              ></img>
              <div className="bg-dark text-center text-white">
                <h5 className="text-warning mb-0  mt-3">{this.state.name}</h5>
                <p className=" m-2">{this.state.price}</p>
                <p style={{ width: "400px" }}>{this.state.desc}</p>
              </div>
            </div>
          </div>
          <img className="m-5" src="./glassesImage/model.jpg"></img>
        </div>

        {/* chọn kính */}
        <div style={{ width: "800px", margin: "auto" }} className="row ">
          {data.map((item, index) => {
            return (
              <div
                className="col-4 d-flex "
                onClick={() => {
                  this.handleChoseGlasses(`${item.url}`, index);
                }}
              >
                <img
                  src={item.url}
                  className="m-3"
                  style={{
                    width: "100px",
                  }}
                />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
